from setuptools import setup

setup(
    name="cas_wrapper",
    version="0.0.2",
    author="Stepan Oksanichenko",
    author_email="soksanichenko@almalinux.org",
    description="The python wrapper around binary cas from "
                "project Codenotary Community Attestation Service.",
    url="https://git.almalinux.org/almalinux/cas_wrapper",
    project_urls={
        "Bug Tracker": "https://git.almalinux.org/almalinux/cas_wrapper/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: "
        "GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
    py_modules=['cas_wrapper'],
    scripts=['cas_wrapper.py'],
    install_requires=[
        'plumbum>=1.7.2',
    ],
    python_requires=">=3.6",
)
